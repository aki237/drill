package main

import (
	"context"
	"drill/pkg/output"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/fatih/color"
	"github.com/urfave/cli"
	yaml "gopkg.in/yaml.v2"
)

// DrillApp struct contains all the variables
// needed for the runtime of the app.
type DrillApp struct {
}

// Build method runs the build sub-command of the application
func (d *DrillApp) Build(ctx *cli.Context) error {
	return d.runFlow(ctx, BUILD)
}

// Clean method runs the clean sub-command of the application
func (d *DrillApp) Clean(ctx *cli.Context) error {
	return d.runFlow(ctx, CLEAN)
}

// runFlow method runs the flow of given FlowType
func (d *DrillApp) runFlow(ctx *cli.Context, ft FlowType) error {
	dconfig, err := readConfig(ctx.String("file"))
	if err != nil {
		return err
	}

	err = dconfig.StartFlow(context.TODO(), ft)
	if err != nil {
		return err
	}

	return nil
}

// Run runs tne partucular build step
func (d *DrillApp) Run(ctx *cli.Context) error {
	dconfig, err := readConfig(ctx.String("file"))
	if err != nil {
		return err
	}

	args := ctx.Args()
	if len(args) == 0 {
		cli.ShowAppHelpAndExit(ctx, 20)
		return nil
	}
	shouldContinue := ctx.Bool("keep-going")
	
	for _, step := range args {
		step, configuration, err := dconfig.ParseStepString(step)
		if err != nil {
			if shouldContinue {
				fmt.Println("Ignoring this error", err.Error())
				continue
			}
			return err
		}
		output.Printf("Running step", color.GreenString(step))
		if err := dconfig.RunStep(context.TODO(), step, configuration, make(Env)); err != nil {
			if shouldContinue {
				fmt.Println("Ignoring this error: ", err.Error())
				continue
			}
			return err
		}
		output.Printf("Done step", color.GreenString(step))
	}

	return nil
}

// readConfig reads the yaml file and returns the DrillConfig
func readConfig(filename string) (*DrillConfig, error) {
	var dconfig DrillConfig

	bc, err := getBytesForFileOrURL(filename)
	if err != nil {
		return nil, err
	}

	err = yaml.Unmarshal(bc, &dconfig)
	if err != nil {
		return nil, err
	}

	if err := resolveImports(&dconfig); err != nil {
		return nil, err
	}

	return &dconfig, nil
}

func resolveImports(dconfig *DrillConfig) error {
	x := &importedSteps{Steps: dconfig.Steps}
	for _, importFile := range dconfig.Import {
		fmt.Printf("Importing file '%s'\n", color.GreenString(importFile))
		bc, err := getBytesForFileOrURL(importFile)
		if err != nil {
			return err
		}

		err = yaml.Unmarshal(bc, &x)
		if err != nil {
			return err
		}
	}

	dconfig.Steps = x.Steps

	return nil
}

func getBytesForFileOrURL(uri string) ([]byte, error) {
	u, err := url.ParseRequestURI(uri)
	if err == nil && u.Scheme != "" {
		return getBytesFromURL(uri)
	}

	return ioutil.ReadFile(uri)
}

func getBytesFromURL(uri string) ([]byte, error) {
	resp, err := http.Get(uri)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	return ioutil.ReadAll(resp.Body)
}
