package main

import "io"

type noErrWriter struct {
	w io.Writer
}

func (ne *noErrWriter) Write(p []byte) (int, error) {
	i, _ := ne.w.Write(p)
	return i, nil
}
