package main

import (
	"drill/pkg/output"
	"fmt"
	"os"
	"time"

	"github.com/fatih/color"
	"github.com/urfave/cli"
)

var (
	version     = "0.0.0"
	gitRevision = ""
	buildDate   = ""
	builderHost = ""
)

func globalMiddleware(ctx *cli.Context, next func(*cli.Context) error) error {
	q := ctx.Bool("quiet")
	if q {
		output.DefaultVerbosity = false
	}
	return next(ctx)
}

func main() {
	cliHandler := cli.NewApp()
	app := &DrillApp{}
	cliHandler.Commands = []cli.Command{
		{
			Name:      "build",
			ShortName: "b",
			Usage:     "Run the build step",
			Action: func(ctx *cli.Context) error {
				return globalMiddleware(ctx, app.Build)
			},
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "f,file",
					Usage: "build configuration file",
					Value: "drill.yaml",
				},
				cli.BoolFlag{
					Name:   "q,quiet",
					Usage:  "disable logging",
					Hidden: false,
				},
			},
		},
		{
			Name:      "watch",
			ShortName: "w",
			Usage:     "Watch for file changes and run the watchFlow",
			Action: func(ctx *cli.Context) error {
				return globalMiddleware(ctx, app.Watch)
			},
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "f,file",
					Usage: "build configuration file",
					Value: "drill.yaml",
				},
				cli.BoolFlag{
					Name:   "q,quiet",
					Usage:  "disable logging",
					Hidden: false,
				},
			},
		},
		{
			Name:      "run",
			ShortName: "r",
			Usage:     "Run the particular build step",
			Action: func(ctx *cli.Context) error {
				return globalMiddleware(ctx, app.Run)
			},
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "f,file",
					Usage: "build configuration file",
					Value: "drill.yaml",
				},
				cli.BoolFlag{
					Name: "k, keep-going",
					Usage: `Continue as much as possible after an error.  While the target that failed, and those that depend on
					it, cannot be remade, the other dependencies of these targets can be processed all the same.`,
					Hidden: false,
				},
				cli.BoolFlag{
					Name:   "q,quiet",
					Usage:  "disable logging",
					Hidden: false,
				},
			},
		},
		{
			Name:      "clean",
			ShortName: "c",
			Usage:     "Run the clean flow",
			Action: func(ctx *cli.Context) error {
				return globalMiddleware(ctx, app.Clean)
			},
			Flags: []cli.Flag{
				cli.StringFlag{
					Name:  "f,file",
					Usage: "build configuration file",
					Value: "drill.yaml",
				},
				cli.BoolFlag{
					Name:   "q,quiet",
					Usage:  "disable logging",
					Hidden: false,
				},
			},
		},
	}

	cliHandler.Author = "Akilan Elango <akilan1997@gmail.com>"
	cliHandler.Description = "drill is simple build tool"
	cliHandler.Name = "drill"
	cliHandler.HelpName = "drill"
	cliHandler.Version = fmt.Sprintf("%s (%s-%s) - buildDate : %s",
		version,
		gitRevision,
		builderHost,
		buildDate,
	)
	cliHandler.Usage = cliHandler.Version

	err := cliHandler.Run(os.Args)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s %s :\t%s\n",
			time.Now().Format("Mon Jan 2 15:04:11 IST 2006"),
			"Flow interrupted",
			color.RedString("%s", err),
		)
	}
}
