package main

import (
	"context"
	"drill/pkg/output"
	"fmt"
	"os"
	"os/signal"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/fatih/color"
	"github.com/fsnotify/fsnotify"
	"github.com/urfave/cli"
)

type matcherConfig struct {
	*regexp.Regexp
	exclude bool
}

type watcherAccumulator struct {
	rxps    []*matcherConfig
	watcher *fsnotify.Watcher
}

func (wacc *watcherAccumulator) walkAndAdd(path string, info os.FileInfo, err error) error {
	if err != nil {
		return err
	}

	// Ignore directories
	if info.IsDir() {
		return nil
	}

	matches := false

	for _, rx := range wacc.rxps {
		if !rx.MatchString(path) {
			continue
		}
		if rx.exclude {
			return nil
		}
		matches = true
	}
	if !matches {
		return nil
	}
	fmt.Printf("Adding '%s'\n", color.BlueString(path))
	err = wacc.watcher.Add(path)
	if err != nil {
		return err
	}

	return nil
}

// Watch method is used to run the watch sub command of the app
func (d *DrillApp) Watch(ctx *cli.Context) error {
	dconfig, err := readConfig(ctx.String("file"))
	if err != nil {
		return err
	}

	if len(dconfig.WatchMatchers) == 0 {
		return fmt.Errorf("watchMatchers is a empty slice")
	}

	rxps, err := getRegexpListForStringList(dconfig.WatchMatchers)
	if err != nil {
		return err
	}

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return err
	}
	defer watcher.Close()

	wacc := &watcherAccumulator{
		rxps:    rxps,
		watcher: watcher,
	}

	err = filepath.Walk(".", wacc.walkAndAdd)
	if err != nil {
		return err
	}

	killsig := make(chan os.Signal, 1)
	canceller := make(chan bool)
	runLoop := true
	signal.Notify(killsig, os.Interrupt)

	go func() {
		<-killsig
		fmt.Print("\r")
		runLoop = false
		canceller <- runLoop
	}()

	for runLoop {
		err := d.runOnce(dconfig, wacc, canceller)
		if err != nil {
			fmt.Println(err)
		}
	}

	return fmt.Errorf("user cancelled watchFlow")
}

func (d *DrillApp) runOnce(dconfig *DrillConfig, wacc *watcherAccumulator, cancelation chan bool) error {
	rctx, cancelFunc := context.WithCancel(context.Background())
	go func(dconf *DrillConfig, context context.Context) {
		err := dconf.StartFlow(context, WATCH)
		if err != nil {
			output.Printf(
				"Flow killed ["+color.RedString(string(WATCH))+"]",
				err,
			)
		}
	}(dconfig, rctx)

	runLoop := true

	for runLoop {
		select {
		case <-cancelation:
			runLoop = false
			continue
		case event := <-wacc.watcher.Events:
			if event.Op != fsnotify.Write {
				continue
			}
			runLoop = false
			continue
		case err := <-wacc.watcher.Errors:
			cancelFunc()
			return err
		}
	}
	cancelFunc()
	<-rctx.Done()
	return nil
}

func getRegexpListForStringList(s []string) ([]*matcherConfig, error) {
	rxps := make([]*matcherConfig, 0)
	for _, wm := range s {
		pattern, exclude := processPattern(wm)
		rt, err := regexp.Compile(pattern)
		if err != nil {
			return nil, err
		}

		rxps = append(rxps, &matcherConfig{
			Regexp:  rt,
			exclude: exclude,
		})
	}

	return rxps, nil
}

// processPattern is used to process the passed string
// as a include or exclude pattern
func processPattern(x string) (pattern string, exclude bool) {
	if strings.HasPrefix(x, "include:") {
		return x[8:], false
	}
	if strings.HasPrefix(x, "exclude:") {
		return x[8:], true
	}

	return x, false
}
