package output

import (
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/fatih/color"
)

var (
	// DefaultVerbosity is a variable that sets the default verbosity
	DefaultVerbosity = true
)

// Printf is an alternative printf for printing logs
func Printf(tag string, content ...interface{}) {
	format := "%s\t" + strings.Repeat("%s ", len(content)) + "\n"
	x := make([]interface{}, 0)
	if DefaultVerbosity {
		format = "%s " + format
		x = append(x, time.Now().Format("Mon Jan 2 15:04:11 IST 2006"))
	}
	x = append(x, tag)
	x = append(x, content...)
	fmt.Printf(format,
		x...,
	)
}

// OutWriter is a private struct that implements the io.Writer interface for colourized output
type OutWriter struct {
	wr          io.Writer
	fmter       *color.Color
	prefix      string
	prefixColor *color.Color
	quiet       bool
}

// NewOutWriter is used to create a new outWriter struct
func NewOutWriter(wr io.Writer, iserr bool, prefix string) io.Writer {
	c := color.New(color.FgWhite)
	if iserr {
		c = color.New(color.FgRed)
	}

	return &OutWriter{
		wr:          wr,
		fmter:       c,
		prefix:      prefix,
		prefixColor: color.New(color.FgYellow, color.Bold),
		quiet:       !DefaultVerbosity,
	}
}

// SetVerbose method used to set the verbosity of logging
func (o *OutWriter) SetVerbose(verbose bool) {
	o.quiet = !verbose
}

// Write implements the io.Writer interface for OutWriter object
func (o *OutWriter) Write(p []byte) (int, error) {
	splits := strings.Split(string(p), "\n")
	for _, val := range splits {
		if strings.TrimSpace(val) == "" {
			continue
		}
		if o.quiet {
			fmt.Fprintf(o.wr, "%s\n", o.fmter.Sprint(val))
			continue
		}
		fmt.Fprintf(o.wr, "%s [%s]\t%s\n",
			time.Now().Format("Mon Jan 2 15:04:11 IST 2006"),
			o.prefixColor.Sprint(o.prefix),
			o.fmter.Sprint(val),
		)
	}
	return len(p), nil
}
