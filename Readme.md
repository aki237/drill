# drill : Simple build system

[![pipeline status](https://gitlab.com/aki237/drill/badges/master/pipeline.svg)](https://gitlab.com/aki237/drill/commits/master)
[![Download Atrifacts](https://d9uh4njw4at4.runkit.sh)](https://gitlab.com/aki237/drill/builds/artifacts/master/browse/artifacts?job=compile)

drill is a simple build system with a readable
configuration. The configuration is in `yaml` and sports a circle CI/gitlab CI like configuration flow.

### Sample configuration

Refer [drill.yaml](drill.yaml).

### Possible Features
 - ~~Build Parallellism~~
 - Remote staging connect
   + No file copy
   + Linux Only
   + network FUSE filesystem to be mounted
   + over gRPC
 - Artifacting and archiving